<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Select Image</name>
   <tag></tag>
   <elementGuidId>9dc657cb-457b-4023-bcb8-e79ffb6763ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '
                                        Select Image
                                        
                                    ' or . = '
                                        Select Image
                                        
                                    ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit your Krista Account'])[1]/following::div[7]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-danger btn-block my-2 rounded file-field waves-effect waves-light</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        Select Image
                                        
                                    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;row mt-3 pt-10 pb-3&quot;]/div[@class=&quot;col-md-6 p-4&quot;]/div[@class=&quot;card pl-3 pr-3&quot;]/div[@class=&quot;card-body card-mini&quot;]/form[1]/div[@class=&quot;uploadPhoto&quot;]/div[@class=&quot;form-group row&quot;]/div[@class=&quot;col-image mb-4 p-0&quot;]/div[@class=&quot;icon-form-size p-0&quot;]/div[@class=&quot;btn btn-danger btn-block my-2 rounded file-field waves-effect waves-light&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit your Krista Account'])[1]/following::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/following::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
