<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>button tab register di bagian atas portal sso</description>
   <name>Button Tab Register</name>
   <tag></tag>
   <elementGuidId>67301faa-c8f4-4332-b381-1ba566540d26</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(text(),'Register')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[contains(text(),'Register')]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//button[contains(text(),'Register')]</value>
   </webElementXpaths>
</WebElementEntity>
