<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Conference Icon</name>
   <tag></tag>
   <elementGuidId>f4fbac15-fe49-40d3-af07-7d6fc79b32a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='scroll1']/div[3]/div/div[7]/button/span/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/media/img/fixed/lobby_button/btn_main_stage.png</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;scroll1&quot;)/div[@class=&quot;ExhibitionHallTemplate_relative__3HO2w&quot;]/div[@class=&quot;ExhibitionHallTemplate_absolute__5MNXI&quot;]/div[7]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MainStageButton_btn--position__3exPM&quot;]/span[@class=&quot;MuiButton-label&quot;]/img[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='scroll1']/div[3]/div/div[7]/button/span/img</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/button/span/img</value>
   </webElementXpaths>
</WebElementEntity>
