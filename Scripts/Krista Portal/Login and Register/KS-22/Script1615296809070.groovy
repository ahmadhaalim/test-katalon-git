import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://portal-krista-review.agatedev.net/')

WebUI.click(findTestObject('Portal Flow/Page_Home Page - Krista Portal/Register as Visitor'))

WebUI.click(findTestObject('Portal Flow/Page_Sign In  Masuk - Krista SSO/Button Tab Register'))

WebUI.verifyElementVisible(findTestObject('Portal Flow/Page_Sign Up  Daftar Baru - Krista SSO/b_Create your Krista Account'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Portal Flow/Page_Sign Up  Daftar Baru - Krista SSO/input_Name_Name'), 'User')

WebUI.setText(findTestObject('Portal Flow/Page_Sign Up  Daftar Baru - Krista SSO/input_Password_Password'), 'Password123')

WebUI.setText(findTestObject('Portal Flow/Page_Sign Up  Daftar Baru - Krista SSO/input_Re-type Password_ConfirmPassword'), 
    'Password123')

WebUI.click(findTestObject('Portal Flow/Page_Sign Up  Daftar Baru - Krista SSO/button_Next'))

WebUI.verifyElementVisible(findTestObject('Portal Flow/Page_Sign Up  Daftar Baru - Krista SSO/Error field/span_The E-mail field is required'))

WebUI.closeBrowser()

