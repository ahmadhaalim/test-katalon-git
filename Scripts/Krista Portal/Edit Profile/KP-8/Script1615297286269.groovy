import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Krista Portal/Login and Register/Login General'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Portal Flow/Page_Home Page - Krista Portal/Dropdown Menu'))

WebUI.click(findTestObject('Portal Flow/Edit Profile/a_Edit Profile'))

WebUI.uploadFile(findTestObject('Portal Flow/Edit Profile/Upload file'), 'C:\\Users\\halim\\Pictures\\tes.jpg')

WebUI.setText(findTestObject('Portal Flow/Edit Profile/input__DisplayName'), 'Halima')

WebUI.click(findTestObject('Portal Flow/Edit Profile/span_Mr'))

WebUI.click(findTestObject('Portal Flow/Edit Profile/li_Mrs'))

WebUI.setText(findTestObject('Portal Flow/Edit Profile/input_land Islands_MobileNumber'), '8123456789')

WebUI.setText(findTestObject('Portal Flow/Edit Profile/input__Company'), 'PT. Coompany')

WebUI.setText(findTestObject('Portal Flow/Edit Profile/input__JobFunction'), 'Staff')

WebUI.click(findTestObject('Portal Flow/Edit Profile/button_Edit'))

WebUI.waitForElementClickable(findTestObject('Portal Flow/Page_Home Page - Krista Portal/Dropdown Menu'), 5)

WebUI.delay(6)

WebUI.click(findTestObject('Portal Flow/Page_Home Page - Krista Portal/Dropdown Menu'))

WebUI.click(findTestObject('Portal Flow/Edit Profile/a_Edit Profile'))

WebUI.verifyElementAttributeValue(findTestObject('Portal Flow/Edit Profile/input__DisplayName'), 'value', 'Halima', 0)

WebUI.verifyElementAttributeValue(findTestObject('Portal Flow/Edit Profile/span_Mr'), 'title', 'Mrs', 0)

WebUI.verifyElementAttributeValue(findTestObject('Portal Flow/Edit Profile/input_land Islands_MobileNumber'), 'value', '812-345-678', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('Portal Flow/Edit Profile/input__Company'), 'value', 'PT. Coompany', 0)

WebUI.verifyElementAttributeValue(findTestObject('Portal Flow/Edit Profile/input__JobFunction'), 'value', 'Staff', 0)

