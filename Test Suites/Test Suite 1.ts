<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite 1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7ab5ea63-8c89-4cbb-83e7-b64eb6f6ac0e</testSuiteGuid>
   <testCaseLink>
      <guid>3fbc4b95-246a-4aa2-a139-da2edd1fe728</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Krista Portal/Login and Register/Login wrong email and password</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8d1f2b50-b17c-4f80-a6b8-2c52101c774d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/New Test Data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>8d1f2b50-b17c-4f80-a6b8-2c52101c774d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>880195e0-9b1d-4682-a63b-d9a8a33dbce0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8d1f2b50-b17c-4f80-a6b8-2c52101c774d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>62f83981-31b4-4716-bc75-24913f3216fd</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>de11e2e0-6209-43f8-a94c-dee74f2db63f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Krista Portal/Login and Register/Empty password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48086336-8c37-4139-ad08-2304ec72be11</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Krista Portal/Login and Register/Login then Logout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae746ebb-a79e-4177-89e8-f9032ffe06fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Krista Portal/Login and Register/Empty email</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
